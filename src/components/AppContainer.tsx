import React from 'react';

const AppContainer: React.StatelessComponent = (props) => {
  return (
    <div className="container-fluid app-container">
      <div className="row">
        {props.children}
      </div>
    </div>
  );
}

export default AppContainer;