import React from 'react';
import './AsideBannerContainer.css';
import Postobon from './../../assets/img/postobon.png';
import Metro from './../../assets/img/metro.png';
import Ciclo from './../../assets/img/ciclo1.png';

const AsideBannerContainer = () => {
  return (
    <div className="col-md-2">
      <div className="container-sidebar-banner">
        <img src={Postobon} className="img-logos-aliados logo-postobon" alt=""/>
        <img src={Metro} alt="" className="img-logos-aliados logo-metro" />
        <img src={Ciclo} alt="" className="img-logos-aliados logo-ciclo" />
      </div>
    </div>
  );  
}

export default AsideBannerContainer;