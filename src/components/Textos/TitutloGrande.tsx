import React, { Fragment } from 'react';
import './Textos.css';

interface Props {
  texto: string;
  color: string;
}

const TituloGrande: React.FC<Props> = ({texto, color}) => {
  return (
    <Fragment>
      <h2 className={"texto-grande text-"+color}>{texto}</h2>
    </Fragment>
  )
}

export default TituloGrande;