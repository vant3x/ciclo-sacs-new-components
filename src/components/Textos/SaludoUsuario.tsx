import React from 'react';
import './SaludoUsuario.css';

const SaludoUsuario:React.FC = () => {
  return (
    <div className="saludo-usuario">
      <h2 className="texto-saludo">Hola</h2>
      <h2 className="bold-text texto-saludo">Mealanie</h2>
    </div>
  );
}

export default SaludoUsuario;