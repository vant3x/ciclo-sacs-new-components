import React, { ReactNode } from 'react';
import styled from 'styled-components';

const BoxShadowContainer = styled.div`
  box-shadow:5px 7px 7px rgba(0,0,0,.22);
  border-radius:20px;
  padding:20px 60px;
  text-align:center;
`;

interface Props {
  margen?: string;
  children: ReactNode;
}


const BoxShadow: React.FC<Props> = ({children, margen = ""}) => {
  return(
    <BoxShadowContainer className={margen}>  
      {children}
    </BoxShadowContainer>
  )
}

export default BoxShadow;