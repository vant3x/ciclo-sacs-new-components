import React, { ReactNode } from 'react';
import styled from 'styled-components';

const BoxShadowContainer = styled.div`
  box-shadow:5px 7px 6px rgba(0,0,0,.18);
  border-radius:18px;
  padding-top:10px;
  padding-left:10px;
  padding-right:10px;
  text-align:center;
  width:220px;
  margin:8px 10px;
  height:100%;
  max-height:90px;
`;

const Titulo = styled.p`
  font-size:1.6rem;
  line-height:.9;
`;

const Valor = styled.p`
  font-weight:bold;
  font-size:1.6rem;
  line-height:.7;

`;

interface Props {
  margen?: string;
  titulo: string;
  valor: number;
  color?:any;
}


const BoxShadowValue: React.FC<Props> = ({titulo, valor, color, margen}) => {
  return(
    <BoxShadowContainer className={margen}>  
      <Titulo>{titulo}</Titulo>
      <Valor className={color}>${valor}{valor !== 0 ?  '.000' : null}</Valor>
    </BoxShadowContainer>
  )
}

export default BoxShadowValue;