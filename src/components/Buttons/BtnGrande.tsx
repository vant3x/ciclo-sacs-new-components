import React, { ReactNode } from 'react';
import './Buttons.css';

interface Props {
  children: ReactNode;
  bg?:any;
  center?:any;
  mt?:any;
}


const BtnGrande: React.FC<Props> = (props) => {
  return (
      <button className={`btn-grande ${props.bg}  ${props.mt} ${props.center ? 'text-align' : null}`}>
        {props.children}
      </button>
  );
}

export default BtnGrande;