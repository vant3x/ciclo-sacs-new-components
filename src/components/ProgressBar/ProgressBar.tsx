import React, { Fragment } from 'react';
import styled from 'styled-components';

interface Props {
 
}

const ContainerProgressBar = styled.div`
  width:64%;
  margin-left:100px;
`; 

const ProgressBarElementContainer = styled.div`
  height:11px;
  background:var(--gris-80);
`;
const ProgressBarElement = styled.div`
  background:var(--verde-claro1) !important;
  width:65%;
`;

const ProgressBar: React.FC<Props> = ({}) => {
  return ( 
    <ContainerProgressBar>
    <ProgressBarElementContainer className="progress">
      <ProgressBarElement className="progress-bar w-65"   role="progressbar" aria-valuenow={65} aria-valuemin={0} aria-valuemax={100}></ProgressBarElement>
    </ProgressBarElementContainer>
    </ContainerProgressBar>
  );
}

export default ProgressBar;