import React, { ReactNode } from 'react';
import styled from 'styled-components';

const BoxShadowContainer = styled.div`
  box-shadow:5px 7px 6px rgba(0,0,0,.18);
  border-radius:13px;
  padding-top:20px !important;
  text-align:center;
  margin:10px;
  background:#fff;
  height:50px;
  display:flex;
  justify-content:center;
  align-items:center;
  width:150px;
`;

const Titulo = styled.p`
  font-size:1.7rem;
  line-height:.9;
`;

const Valor = styled.p`
  font-weight:bold;
  font-size:1.7rem;
  line-height:.7;

`;

interface Props {
  margen?: string;
  cantidad_envases?: number;
  saldo?:any;
}


const BoxShadowXs: React.FC<Props> = (props) => {
  return(
    <BoxShadowContainer className={props.margen}>  
      <Valor>{props.cantidad_envases? props.cantidad_envases : `$${props.saldo}00`}</Valor>
    </BoxShadowContainer>
  )
}

export default BoxShadowXs;