import React from 'react';
import './StatusBar.css';


const StatusBar: React.FC = (props) => {
  return (
    <div className="status-bar-container row">
        <div className="status-bar-text1">
           <p className="status-bar-text ">Estado (#)</p>

        </div>
        <div className="status-bar-text2">
        <p className="status-bar-text">2020 Ingeniería Sostenible Global S.A.S</p>

        </div>
        <div className="status-bar-text3">
        <p className="status-bar-text">Versión XXX</p>

        </div>
    </div>
  )
} 

export default StatusBar;