import React from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import './App.css';
import AppContainer from './components/AppContainer';
import AsideBannerContainer from './components/AsideBanner/AsideBannerContainer';
import Welcome from './pages/Welcome';
import StatusBar from './components/StatusBar/StatusBar';
import Paso2 from './pages/Paso2/Paso2';
import Paso3 from './pages/Paso3/Paso3';

function App() {
  return (
    <div className="App">
      <Router>
        <AppContainer>
          <AsideBannerContainer />
          <div className="col-md-10">
          <Switch>
              <Route exact path="/" component={Welcome} />
              <Route exact path="/paso2" component={Paso2} />
              <Route exact path="/paso3" component={Paso3} />
          </Switch>
          </div>
          <StatusBar/>
        </AppContainer>
      </Router>
    </div>
  );
}

export default App;
