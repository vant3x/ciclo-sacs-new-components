import React from 'react';
import BoxShadowValue from '../../components/BoxShadowValue';
import SaludoUsuario from '../../components/Textos/SaludoUsuario';
import BoxShadowXs from '../../components/BoxShadowXs';
import ProgressBar from '../../components/ProgressBar/ProgressBar';
import BtnGrande from '../../components/Buttons/BtnGrande';
import Pet from './../../assets/img/PET.png';
import Latas from './../../assets/img/Latas.png';
import Money from './../../assets/img/Money.png';

import './Paso2.css';

const Paso2 = () => {
  return (
    <div className="paso2-container">
        <div className="datos-container">
          <SaludoUsuario />
          <BoxShadowValue 
            titulo="Saldo Monedero"
            valor={Number(50.000)}
          />
          <BoxShadowValue 
            titulo="Saldo Banco"
            valor={0}
          />
          <BoxShadowValue 
            titulo="Saldo Crédito"
            valor={0}
            color="valor-red" 
          />

         

        </div>
        <div className="container-info-transaccion">
              <div className="envases-container">
                <img src={Pet} alt=""/>
                <img src={Latas} alt=""/>
                <BoxShadowXs
                  margen=""
                  cantidad_envases={50}
               
                />
                <p className="tipo-valor">Envases</p>
              </div>
              <div className="money-container">
                <img src={Money} alt=""/>
                <BoxShadowXs
                  margen=""
                  saldo={2.500}
                />
                <p className="tipo-valor">Pesos</p>
              </div>
          </div>

          <div className="container-estado-transaccion mt-4">
            <div className="textos-progressbar">
              <p className="texto-envases-maximo">Máximo de envases por transacción</p>
              <p className="numero-envases">50/96 envases</p>
            </div>
            <ProgressBar/>
          </div>
        <div className="center">
          <BtnGrande bg="verde-claro" center={true} mt="mt-3">
                Recargar
            </BtnGrande>
        </div>
    </div>
  )
}

export default Paso2;