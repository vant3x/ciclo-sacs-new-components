import React from 'react';
import TituloGrande from '../components/Textos/TitutloGrande';
import BoxShadow from '../components/BoxShadow';
import './Welcome.css';

const Welcome: React.FC = () => {
  return (
    <div className="welcome-container">
      <TituloGrande 
        texto="¡Bienvenido!" 
        color="green"  
      />
      <BoxShadow margen="margin-top-50 mb-30">
        <h2 className="bold-text texto-box-shadow-civica">Ingresa tu tarjeta Cívica</h2>
        <p className="texto-box-shadow-civica">para comenzar </p>
      </BoxShadow>
      <p>Al ingresar acepta los <a href="">Términos y Condiciones</a></p>
    </div>
  );
}

export default Welcome;