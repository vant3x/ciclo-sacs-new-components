import React from 'react';
import SaludoUsuario from '../../components/Textos/SaludoUsuario';
import BoxShadowValue from '../../components/BoxShadowValue';
import BoxShadowXs from '../../components/BoxShadowXs';
import Pet from './../../assets/img/PET.png';
import Latas from './../../assets/img/Latas.png';
import Money from './../../assets/img/Money.png';
import './Paso3.css'; 

const Paso3: React.FC  = () => {
  return (
    <div className="paso3-container">
      <div className="row-container">
       <SaludoUsuario/>
        <div className="saldos-container">
          <div className="antes-saldo-container mr-4">
            <p className="bold-text titulo-estado">Antes</p>
            <BoxShadowValue
              titulo="Saldo Monedero"
              valor={30.000}
              
            />
             <BoxShadowValue
              titulo="Saldo Banco"
              valor={0}
              margen="mt-4"
            />
              <BoxShadowValue
              titulo="Saldo Crédito"
              valor={0}
              color="valor-red"
              margen="mt-4"

            />
          </div>
          <div className="ahora-saldo-container ml-4">
           <p className="bold-text titulo-estado">Ahora</p>
              <BoxShadowValue
                titulo="Saldo Monedero"
                valor={32.}
                color="valor-green"
              />
               <BoxShadowValue
                titulo="Saldo Banco"
                valor={0}
                color="valor-green"
                margen="mt-4"
              />
              <BoxShadowValue
              titulo="Saldo Crédito"
              valor={0}
              color="valor-green"
              margen="mt-4"
            />
          </div>
          <div className="container-info-final-transaccion">
              <div className="envases-latas-container">
                <div className="img-envases-latas">
                  <img className="pet" src={Pet} alt=""/>
                  <img  className="latas" src={Latas} alt=""/>
                </div>
                <BoxShadowXs
                  cantidad_envases={50}
                />
                <p className="tipo-valor">
                  Envases
                </p>
              </div>
              <div className="saldo-final-container">
                <img className="money-img" src={Money} alt=""/>
                <BoxShadowXs
                  saldo={2.50}
                />
                <p className="tipo-valor">
                  Pesos
                </p>
              </div>
          </div>
        </div>
      </div>
    </div>
    );
}

export default Paso3;